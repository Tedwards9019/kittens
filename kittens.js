var taskWatch;
if(typeof(taskWatch) != Array)
{
    taskWatch = [];
}
else 
{
    taskWatch.forEach(element => {
        clearInterval(element);
    });
}
(
()=>{
        var fastQueue = [];
        var mediumQueue = [];
        var slowQueue = [];

        var parseSi = (val)=>{
            var num = Number.parseFloat(val);
            var si = val.substring(val.length-1);
            switch(si)
            {
                case"K":
                num = num * Math.pow(10,3);
                break;
            }
            return num;
        }

        var parseNode = (id)=>{
            var node = document.querySelector(`[data-reactid="${id}"]`);
            if(node.children.length == 0)
            return false;

            return {
                pool : parseSi(node.children[1].textContent),
                rate : Number.parseFloat(node.children[3].textContent.replace(/[()/+sec]/g,'')),
                max : parseSi(node.children[2].textContent.substring(1)),
                type : node.children[0].textContent
            }
        

        };

        var maxConvert = (node,action)=>{
            return ()=>{
                var n = parseNode(node);
                if(n == false)
                return;
                
                if((n.pool + (n.rate * 2)) > n.max)
                    document.querySelector(`[data-reactid="${action}"]`).click();
            };
        }

        if(document.querySelector('#folfStats') == undefined)
        //document.querySelector('#midColumn').append(object.apply(document.createElement('div'),{id:"folfStats"}));

        slowQueue.push(maxConvert('.0.0.1.1.b','.0.2.0')); //cat power auto hunt
        slowQueue.push(maxConvert('.0.0.1.1.d','.0.4.1.0.f.6.0')) //culture to manuscripts
        slowQueue.push(maxConvert('.0.0.1.1.0','.0.4.1.0.0.5.0')); //capnip autoconvert
        slowQueue.push(maxConvert('.0.0.1.1.1','.0.4.1.0.2.6.0'));
        slowQueue.push(maxConvert('.0.0.1.1.2','.0.4.1.0.3.6.0'));
        //coal
        slowQueue.push(maxConvert('.0.0.1.1.3','.0.4.1.0.5.6.0'));
        //iron
        slowQueue.push(maxConvert('.0.0.1.1.4','.0.4.1.0.4.6.0'));

        slowQueue.push(()=>{
            var observe = document.querySelector('#observeBtn');

            if(observe != undefined)
            observe.click();

        });

        var UOW = (taskList)=>{return ()=>{taskList.forEach(v=>v.call())}};
        taskWatch.push(setInterval(UOW(fastQueue),50));
        taskWatch.push(setInterval(UOW(mediumQueue),200));
        taskWatch.push(setInterval(UOW(slowQueue),600));
})();